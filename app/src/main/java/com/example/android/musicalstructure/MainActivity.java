package com.example.android.musicalstructure;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Find the View that shows the list of artists screen
        TextView artistList = (TextView) findViewById(R.id.artist_list);

        // Set a click listener on that View
        artistList.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the list of artists button is clicked on.
            @Override
            public void onClick(View view) {
                Intent artistsListIntent = new Intent(MainActivity.this, ArtistListActivity.class);
                startActivity(artistsListIntent);
            }
        });

        // Find the View that shows the search screen
        TextView search = (TextView) findViewById(R.id.search);

        // Set a click listener on that View
        search.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the search button is clicked on.
            @Override
            public void onClick(View view) {
                Intent searchIntent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(searchIntent);
            }
        });
        // Find the View that shows the details screen
        TextView details = (TextView) findViewById(R.id.details);

        // Set a click listener on that View
        details.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the details button is clicked on.
            @Override
            public void onClick(View view) {
                Intent detailsIntent = new Intent(MainActivity.this, DetailsActivity.class);
                startActivity(detailsIntent);
            }
        });

        // Find the View that shows the payment screen
        TextView payment = (TextView) findViewById(R.id.payment);

        // Set a click listener on that View
        payment.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the details button is clicked on.
            @Override
            public void onClick(View view) {
                Intent paymentIntent = new Intent(MainActivity.this, PaymentActivity.class);
                startActivity(paymentIntent);
            }
        });

    }
}
