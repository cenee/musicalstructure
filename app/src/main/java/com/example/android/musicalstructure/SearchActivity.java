package com.example.android.musicalstructure;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Find the View that shows the list of artists screen
        TextView findMusic = (TextView) findViewById(R.id.find_music);

        // Set a click listener on that View
        findMusic.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the list of artists button is clicked on.
            @Override
            public void onClick(View view) {
                Intent artistsListIntent = new Intent(SearchActivity.this, ArtistListActivity.class);
                startActivity(artistsListIntent);
            }
        });
        // Find the View that shows the main screen with currently playing music
        TextView back = (TextView) findViewById(R.id.back);

        // Set a click listener on that View
        back.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the back button is clicked on.
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(SearchActivity.this, MainActivity.class);
                startActivity(mainIntent);
            }
        });
    }
}
