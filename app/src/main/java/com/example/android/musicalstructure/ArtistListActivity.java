package com.example.android.musicalstructure;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ArtistListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_list);

        // Find the View that shows the search screen
        TextView search = (TextView) findViewById(R.id.search_again);

        // Set a click listener on that View
        search.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the search button is clicked on.
            @Override
            public void onClick(View view) {
                Intent searchIntent = new Intent(ArtistListActivity.this, SearchActivity.class);
                startActivity(searchIntent);
            }
        });

        // Find the View that shows the main screen with currently playing music and play it
        TextView main = (TextView) findViewById(R.id.play_now);

        // Set a click listener on that View
        main.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the play now button is clicked on.
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(ArtistListActivity.this, MainActivity.class);
                startActivity(mainIntent);
            }
        });

        // Find the View that shows the main screen with currently playing music
        TextView back = (TextView) findViewById(R.id.back);

        // Set a click listener on that View
        back.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the back button is clicked on.
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(ArtistListActivity.this, MainActivity.class);
                startActivity(mainIntent);
            }
        });
    }
}
